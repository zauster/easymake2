easymake2
================

{easymake2} is a package that analyses the inputs and outputs of R
scripts and writes a Makefile from that information. It can also plot
the directed acyclical graph (DAG) that is implied by the dependencies
between the scripts.

This package is a rewrite of another package called {easyMake}, which is
available [here](https://github.com/GShotwell/easyMake) (the original
package) and [here](https://github.com/zauster/easyMake) (which was my
slightly enhanced version). {easyMake} was a proof of concept for a
simple way to generate Makefiles based on an R dataframe listing file
dependencies.

Out of fun and wanting to set the package up properly, I rewrote that
package. The idea behind the package is the same and the functionality
matches the ‘old’ package.

## Installation

It is not on CRAN, but you can install it with:

``` r
devtools::install_github("zauster/easymake2")
```

## Usage

easymake2 provides functions that analyse the inputs and outputs of
R-scripts in a project directory and can create a Makefile from it.

The Makefile can then be used to execute all R-scripts in order. Even
better, it *only* executes those R-scripts whose imports have changed,
resulting in up-to-date outputs. R-scripts whose inputs did not change
are not executed and execution time is saved.

``` r
## create the data.frame with the inputs/outputs of the R-scripts
deps <- detect_dependencies()

## write the Makefile out
write_makefile(deps)
```

[GNU Make](https://www.gnu.org/software/make/) is needed, it reads the
Makefile and takes care of the execution. Simply run

``` sh
make
```

from the command line. Nice by-products of this is that you can tell GNU
Make to execute the scripts in parallel, e.g. to use two processes:

``` sh
make -j2
```

For additional inspection, the dependencies between the scripts and
their inputs/outputs can be plotted with:

``` r
plot_dependencies(deps)
```

This function will create a
[DOT](https://graphviz.org/doc/info/lang.html) file which contains the
description of a graph. The graph is then rendered with `dot` to produce
a PDF file (thus the `dot` executable has to be installed and must be
found by R). An example output of a test project (located under
tests/testdata/test_project_medium):

![A graph produced by `plot_dependencies`](Makegraph.png)

## Best practices

### Write R scripts that do one *task*, with clear inputs and outputs

E.g. one script to read a `csv` file, clean it and save it to an `RData`
file. The next script loads the `RData` file, does some transformations
and saves it again. The next script then runs a model, does some
estimations, etc. Finally an `Rmd` file that collects the results.

### Help `detect_dependencies` detect the dependencies

-   Do not use the same names for inputs and outputs: this creates a
    cycle in the Makefile and makes it unusable

``` r
## DO
load("data/input.RData")
...
save("data/output.RData")

## DO NOT
load("data/tmp.RData")
...
save("data/tmp.RData")
```

-   Use clear and full filenames, do not import or export data
    programatically and do not reset the working directory within your
    script (see also the nice package
    [{here}](https://cran.r-project.org/package=here) to set the working
    directory to the project root).

``` r
## DO
library(here) ## optional, but recommmended
setwd(here()) ## optional, but recommmended
read.csv("data/path/to/file.csv")

## DO NOT
csvfile <- "data/path/to/file.csv"
read.csv(csvfile)

## DO NOT
setwd("data/path/to/")
read.csv("file.csv")
```

Explanation: You have to use the full path relative to the directory
where the Makefile will be written to (usually the project root): GNU
Make needs to know where the inputs and outputs are and whether they
have changed. If you re-set the working directory in a script and then
load the inputs from there, GNU Make will not find those inputs.

-   Try not to use a for-loop for importing or exporting data,
    {easymake2} will not be able to detect the correct filename. Use a
    list of inputs/output, if possible.

``` r
## DO, in file1.R
output_list <- list(output1 = ...,
                    output2 = ...)
save(output_list, file = "data/output_list.RData")

## DO, in file2.R
load("data/output_list.RData")


## DO NOT
for(x in output_list) {
   save(x, file = paste0("data/", ..., ".RData"))
}
```

-   If loops over files cannot be avoided: Create additional
    helper-imports and helper-exports after the export for-loop and
    before the import for-loop. This creates the correct dependency
    between the script files.

``` r
## DO, in file1.R
for(x in list) { save(...) }
current_time <- Sys.time()
save(current_time, file = "data/ScriptA_finished.RData")

## DO, in file2.R
load("data/ScriptA_finished.RData")
for(x in list) { load(...) }
```
