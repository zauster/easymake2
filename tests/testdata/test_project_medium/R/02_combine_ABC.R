

##
## read stuff from previous scripts
##
load("Data/partA.RData")
load("Data/partB.RData")

partC <- readRDS("Data/partC.rds")



##
## do some magic combination of the inputs
combined_ABC <- partC

## add a bogus read in a comment
## partC <- readRDS("Data/partC.rds")
## source("R/01_inputC.R")




##
## Tadaaa...
##
## here we have some final output
write.csv2(combined_ABC, file = "Data/Step2_csv2_output.csv")
