context("parse file")
library(data.table)

setwd("../testdata/test_project_medium/")

import_functions <- c("fread", "import", "load",
                      "read_delim", "read_file", "read_fw",
                      "read_lines", "read_log",
                      "read_table", "read.csv", "read.dta",
                      "read.systat", "read.table",
                      "read.xlsx", "read_excel", "readRDS",
                      "read.fst", "read_fst")
export_functions <- c("export", "fwrite", "save", "saveRDS",
                      "ggsave", "pdf", "write_csv",
                      "write.csv", "write.csv2",
                      "write.dcf", "write.dta",
                      "write.foreign", "write.ftable",
                      "write.table", "write.fst",
                      "write_fst", "write.tsv",
                      "write.xlsx")

fname <- "R/01_inputA.R"
file_deps <- parse_file(fname, import_functions, export_functions)

test_that("inputA.R dependencies are correctly detected", {
  expect_equal(nrow(file_deps), 4)
  expect_equal(nrow(file_deps[prereq %like% "csv_input.csv"]), 1)
  expect_equal(nrow(file_deps[prereq %like% "stata_input.dta"]), 1)
  expect_equal(nrow(file_deps[prereq %like% "excel_input.xlsx"]), 1)
  expect_equal(nrow(file_deps[prereq %like% "inputA.R"]), 1)
  expect_equal(nrow(file_deps[target %like% "inputA.R"]), 3)
})

fname <- "R/01_inputB.R"
file_deps <- parse_file(fname, import_functions, export_functions)
test_that("inputB.R dependencies are correctly detected", {
  expect_equal(nrow(file_deps), 2)
  expect_equal(nrow(file_deps[prereq %like% "input1.RData"]), 1)
  expect_equal(nrow(file_deps[target %like% "inputB.R"]), 1)
})

fname <- "R/04_Report.Rmd"
file_deps <- parse_file(fname, import_functions, export_functions)
test_that("04_Report.Rmd dependencies are correctly detected", {
  expect_equal(nrow(file_deps), 2)
  ## test that the inputs do not start with ../
  expect_equal(nrow(file_deps[prereq %like% "^Data"]), 2)
  expect_equal(nrow(file_deps[target %like% "Rmd$"]), 2)
})
